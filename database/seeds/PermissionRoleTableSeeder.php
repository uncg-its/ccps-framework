<?php

use Illuminate\Database\Seeder;
use \App\Role;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // laratrust configuration

        $permissionRoleArrayConstruction = ["role_id" => ""];

        $permissionRoleShell = [
            "role_id" => "1",
        ];

        // validate
        $valid = true;

        $diff = array_diff_key($permissionRoleShell, $permissionRoleArrayConstruction);
        if (!empty($diff)) {
            $valid = false;
            if ($writeConsoleOutput) {
                $output->text('Cannot seed - source data format is invalid.');
                return;
            }
        }

        for($x=1; $x<=10; $x++) {
            $mappingArray = array_merge($permissionRoleShell, ['permission_id' => $x]);
            DB::table('ccps_permission_role')->insert($mappingArray);
        }
    }
}
