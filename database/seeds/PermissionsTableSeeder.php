<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // laratrust configuration

        $permissionArrayConstruction = ["name" => "", "display_name" => "", "description" => ""];

        $permissions = [
            // user list
            [
                "name" => "users-view",
                "display_name" => "Users - View",
                "description" => "View the user list and details"
            ],
            [
                "name" => "users-edit",
                "display_name" => "Users - Edit",
                "description" => "Edit users of the application and their roles / permissions"
            ],
            [
                "name" => "users-delete",
                "display_name" => "Users - Delete",
                "description" => "Delete users of the application"
            ],
            [
                "name" => "users-create",
                "display_name" => "Users - Create",
                "description" => "Create new users in the application"
            ],

            // ACL
            [
                "name" => "acl-view",
                "display_name" => "ACL - View",
                "description" => "View the application ACL (roles and permissions)"
            ],
            [
                "name" => "acl-edit",
                "display_name" => "ACL - Edit",
                "description" => "Edit the application ACL (roles and permissions)"
            ],
            [
                "name" => "acl-delete",
                "display_name" => "ACL - Delete",
                "description" => "Delete roles and permissions from the ACL"
            ],
            [
                "name" => "acl-create",
                "display_name" => "ACL - Create",
                "description" => "Create new roles and permissions in the ACL"
            ],

            // Application Config
            [
                "name" => "config-view",
                "display_name" => "Application Configuration - View",
                "description" => "View the application's configuration variables"
            ],
            [
                "name" => "config-edit",
                "display_name" => "Application Configuration - Edit",
                "description" => "Edit the application's configuration variables"
            ],
        ];

        // validate
        $valid = true;
        foreach($permissions as $index => $permissionArray) {
            $diff = array_diff_key($permissionArray, $permissionArrayConstruction);
            if (!empty($diff)) {
                $valid = false;
                if ($writeConsoleOutput) {
                    $output->text('Cannot seed - source data format is invalid at index ' . $index);
                    return;
                }
            }
        }

        $existingRoles = Permission::all();
        foreach($permissions as $permissionArray) {
            if ($existingRoles->contains("name", $permissionArray["name"])) {
                if ($writeConsoleOutput) {
                    $output->text('Skipping index with name ' . $permissionArray["name"] . ' - already exists in table');
                }
            } else {
                $insertArray = array_merge($permissionArray, [
                    'created_at' => date("Y-m-d H:i:s", time()),
                    'updated_at' => date("Y-m-d H:i:s", time()),
                    'editable' => 0
                ]);

                DB::table('ccps_permissions')->insert($insertArray);
            }
        }
    }
}
