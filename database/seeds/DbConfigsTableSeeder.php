<?php

use Illuminate\Database\Seeder;

class DbConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $configs = [
            "debugBar" => "disabled",
            "theme" => "base"
        ];


        foreach($configs as $key => $value) {
            \App\DbConfig::updateOrCreate(['key' => $key], ['value' => $value]);
        }

    }
}
