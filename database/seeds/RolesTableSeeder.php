<?php

use Illuminate\Database\Seeder;
use \App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // laratrust configuration

        $roleArrayConstruction = ["name" => "", "display_name" => "", "description" => ""];

        $roles = [
            [
                "name" => "admin",
                "display_name" => "Administrator",
                "description" => "Application Administrator"
            ],
            [
                "name" => "authenticated-user",
                "display_name" => "Authenticated User",
                "description" => "Default logged-in user"
            ],
        ];

        // validate
        $valid = true;
        foreach($roles as $index => $roleArray) {
            $diff = array_diff_key($roleArray, $roleArrayConstruction);
            if (!empty($diff)) {
                $valid = false;
                if ($writeConsoleOutput) {
                    $output->text('Cannot seed - source data format is invalid at index ' . $index);
                    return;
                }
            }
        }

        $existingRoles = Role::all();
        foreach($roles as $roleArray) {
            if ($existingRoles->contains("name", $roleArray["name"])) {
                if ($writeConsoleOutput) {
                    $output->text('Skipping index with name ' . $roleArray["name"] . ' - already exists in table');
                }
            } else {
                $insertArray = array_merge($roleArray, [
                    'created_at' => date("Y-m-d H:i:s", time()),
                    'updated_at' => date("Y-m-d H:i:s", time()),
                    'editable' => 0
                ]);

                DB::table('ccps_roles')->insert($insertArray);
            }
        }
    }
}
