<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application variables
    |--------------------------------------------------------------------------
    |
    | Application variables. Not sure if these are good or not.
    |
    */

    'search' => env('APP_SEARCH', false), // should we show the search?

    'login_methods' => explode(',', env('APP_LOGIN_METHODS', 'local')),

    'allow_signups' => env('APP_ALLOW_SIGNUPS', 'false') == 'true',

];
