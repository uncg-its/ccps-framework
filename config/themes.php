<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Switch this package on/off. Useful for testing...
	|--------------------------------------------------------------------------
	*/

	'enabled' => true,

    /*
    |--------------------------------------------------------------------------
    | File path where themes will be located.
    | Can be outside default views path EG: resources/themes
    | Leave it null if you place your themes in the default views folder 
    | (as defined in config\views.php)
    |--------------------------------------------------------------------------
    */

//    'themes_path' => null, // eg: realpath(base_path('resources/themes'))
    'themes_path' => realpath(base_path('resources/themes')),

	/*
	|--------------------------------------------------------------------------
	| Set behavior if an asset is not found in a Theme hierarchy.
	| Available options: THROW_EXCEPTION | LOG_ERROR | ASSUME_EXISTS | IGNORE
	|--------------------------------------------------------------------------
	*/

	'asset_not_found' => 'LOG_ERROR',

	/*
	|--------------------------------------------------------------------------
	| Set the Active Theme. Can be set at runtime with:
	|  Themes::set('theme-name');
	|--------------------------------------------------------------------------
	*/

	'active' => 'base', // default value if none is specified in the view or controller (or db_configs table).

	/*
	|--------------------------------------------------------------------------
	| Define available themes. Format:
	|
	| 	'theme-name' => [
	| 		'extends'	 	=> 'theme-to-extend',  // optional
	| 		'views-path' 	=> 'path-to-views',    // defaults to: resources/views/theme-name
	| 		'asset-path' 	=> 'path-to-assets',   // defaults to: public/theme-name
	|
	|		// you can add your own custom keys and retrieve them with Theme::config('key');
	| 		'key' 			=> 'value', 
	| 	],
	|
	|--------------------------------------------------------------------------
	*/

	'themes' => [

		'base' => [
            'displayName'  => 'CCPS Base',
		    'extends'	 	=> null,
            'views-path' 	=> 'base/views',
            'asset-path' 	=> 'base/assets',
        ],

        'admin-lte' => [
            'displayName'  => 'Admin-LTE',
            'extends'	 	=> 'base',
            'views-path' 	=> 'admin-lte/views',
            'asset-path' 	=> 'admin-lte/assets',
        ],

        'uncg' => [
            'displayName'  => 'UNCG Wrapper',
            'extends'	 	=> 'base',
            'views-path' 	=> 'uncg/views',
            'asset-path' 	=> 'uncg/assets',
        ]

	],

];
