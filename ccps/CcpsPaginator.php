<?php

namespace Ccps;

use Illuminate\Pagination\LengthAwarePaginator;

class CcpsPaginator extends LengthAwarePaginator {

    public $startingRecord, $endingRecord;

    public function __construct($entireCollection, $perPage = null, $currentPage = null) {
        if (is_null($perPage)) {
            $perPage = config('app.paginator_per_page');
        }

        if (is_null($currentPage)) {
            $currentPage = request()->page ?: 1;
        }

        $totalRecords = $entireCollection->count();

        // calculate starting and ending
        $this->startingRecord = ($currentPage - 1) * $perPage + 1;
        $this->endingRecord = $this->startingRecord + ($perPage - 1);
        if ($this->endingRecord > $totalRecords) {
            $this->endingRecord = $totalRecords;
        }

        parent::__construct(
            $entireCollection->forPage($currentPage, $perPage),
            $totalRecords,
            $perPage,
            $currentPage,
            ['path' => self::resolveCurrentPath()]
        );

    }

    public function header($noun = "record") {
        $noun = str_plural($noun);
        return "<h4>Displaying {$noun} {$this->startingRecord} - {$this->endingRecord} of {$this->total()} total</h4>";
    }

}