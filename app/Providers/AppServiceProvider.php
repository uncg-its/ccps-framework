<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    public function getUserData($fakeUser = false) {
        // dummy data
        $userNotifications = [
            "messages"      => ["test", "test", "test", "test"],
            "notifications" => ["test", "test", "test", "test", "test", "test", "test", "test", "test"],
            "tasks"         => ["test", "test", "test", "test", "test", "test"]
        ];

        if (Auth::check()) {
            $user = Auth::user();
        } else {
            if ($fakeUser) {

            // get fake user data
            $faker = Factory::create();

            $user = new \stdClass();
            $user->name = $faker->name;
            $user->email = $faker->email;
            $user->avatar_url = 'http://lorempixel.com/200/200/cats';
            $user->created_at = Carbon::now();

            } else {
                $user = false;
            }
        }

        return [
            "user" => $user,
            "notifications" => $userNotifications
        ];
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function($view) {

            $userData = $this->getUserData();

            $view->with('user', $userData["user"]);
            $view->with('notifications', $userData["notifications"]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
