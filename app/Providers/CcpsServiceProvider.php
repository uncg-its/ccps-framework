<?php

namespace App\Providers;

use App\DbConfig;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class CcpsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // fix database for older MySQL
        Schema::defaultStringLength(191);

        if (Schema::hasTable('ccps_db_configs')) {
            // bind config variables to the container
            $this->app->singleton('dbConfig', function () {
                return DbConfig::getConfigCollection();
            });

            // have to run this here in boot() to get access to config from database
            if ($this->app->environment() !== 'production' && app('dbConfig')->get('debugBar') == "enabled") {
                $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
                $this->app->alias('Debugbar', \Barryvdh\Debugbar\Facade::class);
            }
        }

        // register custom Blade directives

        // Call to Laratrust::hasRole
        Blade::directive('norole', function ($expression) {
            return "<?php if (!app('laratrust')->hasRole({$expression})) : ?>";
        });

        // Call to Laratrust::can
        Blade::directive('nopermission', function ($expression) {
            return "<?php if (!app('laratrust')->can({$expression})) : ?>";
        });

        // Call to Laratrust::ability
        Blade::directive('noability', function ($expression) {
            return "<?php if (!app('laratrust')->ability({$expression})) : ?>";
        });

        /* NOT SURE IF WE NEED THESE

        // Call to Laratrust::hasRole
        Blade::directive('norole', function ($expression) {
            return "<?php if (!app('laratrust')->hasRole{$expression}) : ?>";
        });

        // Call to Laratrust::can
        Blade::directive('nopermission', function ($expression) {
            return "<?php if (!app('laratrust')->can{$expression}) : ?>";
        });

        // Call to Laratrust::ability
        Blade::directive('noability', function ($expression) {
            return "<?php if (!app('laratrust')->ability{$expression}) : ?>";
        });
        */

        Blade::directive('endnorole', function () {
            return "<?php endif; // !app('laratrust')->hasRole ?>";
        });

        Blade::directive('endnopermission', function () {
            return "<?php endif; // !app('laratrust')->can ?>";
        });

        Blade::directive('endnoability', function () {
            return "<?php endif; // !app('laratrust')->ability ?>";
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            // load development-only service providers and facades
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
