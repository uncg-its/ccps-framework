<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // add custom validation rules here. these are global; recommend putting validation information for single requests inside of the request object's 'moreValidation()' method.

        /*
        $this->app['validator']->extend('myglobalrule', function ($attribute, $value, $parameters)
        {
            // insert logic here

            return true; // passes validation
        }, 'validation failure message');
        */
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
