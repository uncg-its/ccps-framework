<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Laratrust;
use Menu;

class MenuServiceProvider extends ServiceProvider {
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        //
    }
}
