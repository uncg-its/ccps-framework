<?php

namespace App\Http\Middleware;

use Closure;
use igaster\laravelTheme\Facades\Theme;

class SetThemeFromSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // session set?
        if (session()->has('theme')) {
            Theme::set(session('theme'));
        }

        return $next($request);
    }
}
