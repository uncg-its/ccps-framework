<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Laratrust;
use Menu;

class DefineAppMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // define application menus here

        Menu::make('nav', function($menu) {
            // base for unauthenticated users
            $menu->add('Home');

            // for logged-in users
            if ($user = Auth::user()) {
                $takeUsersActions = [
                    'users-view',
                    'users-edit',
                    'users-delete',
                    'users-create',
                ];
                $takeAclActions = [
                    'acl-view',
                    'acl-edit',
                    'acl-delete',
                    'acl-create',
                ];
                $takeConfigActions = [
                    'config-view',
                    'config-edit',
                ];

                $takeCustomActions = [
                    // add any custom permissions that should result in the user seeing the Admin menu
                ];

                $seeAdminMenu = array_merge($takeUsersActions, $takeAclActions, $takeConfigActions, $takeCustomActions);

                // NEW - granular permissions
                if ($user->can($seeAdminMenu)) {
                    $menu->add('Admin', url('/admin'));

                    if ($user->can($takeUsersActions)) {
                        $menu->admin->add('Users', url('/admin/users'));
                    }
                    if ($user->can($takeAclActions)) {
                        $menu->admin->add('ACL', url('/admin/acl'));
                    }
                    if ($user->can($takeConfigActions)) {
                        $menu->admin->add('Config', url('/admin/config'));
                    }
                }

                // Demo items
                $menu->add('Demo', url('/demo'));
                $menu->demo->add('Themes', url('/demo/themes'));
                $menu->demo->add('UNCG Theme', url('/demo/theme/uncg'));
                $menu->demo->add('Flash Messages', url('/demo/flash'));
                $menu->demo->add('Breadcrumbs', url('/demo/breadcrumbs'));
                $menu->demo->add('Guzzle', url('/demo/guzzle'));
                $menu->demo->add('Menu', url('/demo/menu'));
                $menu->demo->add('Email', url('/demo/email'));

            }

        });

        return $next($request);
    }
}
