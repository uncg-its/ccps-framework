<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdminPrivs
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();
        if (!$user || !$user->hasRole('admin')) {
            abort(403, 'You are not an administrator.');
        }

        return $next($request);
    }
}
