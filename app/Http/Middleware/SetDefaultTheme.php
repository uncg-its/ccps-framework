<?php

namespace App\Http\Middleware;

use App\DbConfig;
use Closure;
use igaster\laravelTheme\Facades\Theme;

class SetDefaultTheme
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // session set?
        $currentTheme = app('dbConfig')->get('theme');
        if (!is_null($currentTheme)) {
            Theme::set($currentTheme);
        }

        return $next($request);
    }
}
