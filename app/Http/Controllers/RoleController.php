<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleCreateRequest;
use App\Http\Requests\RoleUpdateRequest;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller {

    public function __construct() {
        $this->middleware('permission:acl-view')->only(['index','show']);
        $this->middleware('permission:acl-edit')->only(['edit','update']);
        $this->middleware('permission:acl-delete')->only('destroy');
        $this->middleware('permission:acl-create')->only(['create','store']);
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index() {
        $roles = Role::all();
        return view('acl.roles.index')->with(compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create() {
        $permissions = Permission::all();
        return view('acl.roles.create')->with(array_merge(compact('permissions'), ["role" => new Role]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     */
    public function store(RoleCreateRequest $request) {
        $role = $request->persist();

        return redirect('/admin/acl/roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  Role $role
     */
    public function show(Role $role) {
        $role->load(['users', 'permissions']);
        return view('acl.roles.show')->with(compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Role $role
     */
    public function edit(Role $role) {
        $permissions = Permission::all();
        return view('acl.roles.edit')->with(compact(['role', 'permissions']));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Role $role
     */
    public function update(RoleUpdateRequest $request, Role $role) {
        $role = $request->persist($role);

        return redirect('/admin/acl/role/' . $role->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Role $role
     */
    public function destroy(Role $role) {
        if ($role->delete()) {
            flash("Role '$role->display_name' deleted successfully.", 'success');
        } else {
            flash("Role '$role->display_name' could not be deleted.", 'danger');
        }

        return redirect('/admin/acl/roles');
    }
}
