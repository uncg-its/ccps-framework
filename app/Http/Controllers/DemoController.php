<?php

namespace App\Http\Controllers;

use App\Mail\Demo;
use Carbon\Carbon;
use GuzzleHttp\Client;
use igaster\laravelTheme\Facades\Theme;
use Illuminate\Http\Request;
use \Faker\Factory;
use Illuminate\Support\Facades\Auth;
use StanDaniels\Flash\Flash;

class DemoController extends Controller {

    public function __construct() {
        // theme can be set controller-wide
//        $this->middleware('SetThemeFromSession');
    }

    public function index() {
        return view('index');
    }

    public function themes() {
        return $this->showTheme();
    }

    public function theme($themeName) {
        // theme can be set per-route as well
        return $this->showTheme($themeName);
    }

    public function setTheme(Request $request) {
        $theme = $request->theme;
        if ($theme == "clear") {
            session()->forget('theme');
        } else {
            session(["theme" => $request->theme]);
        }

        return redirect('/demo/themes');
    }

    protected function showTheme($overrideTheme = "") {
        // do we override theme?
        if ($overrideTheme != "") {
            Theme::set($overrideTheme);
        }

        return view('theme');
    }

    public function flash() {
        return view('flash');
    }

    public function flashMessages(Request $request) {

        // flash the message(s)
        $message = $request->message;
        $type = $request->type;

        $possibleTypes = ["success", "info", "warning", "danger"];

        if ($type != "all" && !in_array($type, $possibleTypes)) {
            throw new \Exception("Error: invalid type");
        }

        $typesToFlash = $type == "all" ? $possibleTypes : [$type];

        foreach ($typesToFlash as $flash) {
            flash($message, $flash);
        }

        return redirect('/');
    }

    public function breadcrumbs() {

        return view('breadcrumbs');
    }

    public function guzzle() {

        $client = new Client();

        $res = $client->request('GET', 'https://httpbin.org/get', [
//            'auth' => ['user', 'pass']
        ]);

        $result = $res->getBody()->getContents();

        return view('guzzle')->with(compact('result'));
    }

    public function dashboard() {
        $user = Auth::user();
        return view('dashboard')->with(compact('user'));
    }

    public function menu() {
        $user = Auth::user();
        return view('menu')->with(compact('user'));

    }

    public function email() {
        $user = Auth::user();
        return view('email')->with(compact('user'));
    }

    public function sendEmail(Request $request) {
        $this->validate($request, [
            'to' => 'required|email',
            'message' => 'required'
        ]);

        try {
            \Mail::to($request->to)->send(new Demo($request->message));
            flash('Email sent successfully.', 'success');
        } catch (\Exception $e) {
            flash('Error sending email: ' . $e->getMessage());
        }

        return redirect("/");
    }

}
