<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    protected $user;

    public function __construct() {
        $this->middleware('auth');

        $this->user = Auth::user();
    }

    public function index() {
        return view('account.index')->with(["userToShow" => $this->user]);
    }

    public function profile() {
        return view('account.profile.show')->with(["userToShow" => $this->user]);
    }

    public function editProfile() {
        return view('account.profile.edit')->with(["userToShow" => $this->user]);
    }

    public function settings() {
        return view('account.settings')->with(["userToShow" => $this->user]);
    }
}
