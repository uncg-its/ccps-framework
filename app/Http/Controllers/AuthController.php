<?php

namespace App\Http\Controllers;

use Auth;
use Laravel\Socialite\Facades\Socialite;
use App\User;

class AuthController extends Controller
{
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Redirect the user to the OAuth Provider.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        $hd = env('SOCIALITE_ALLOW_ONLY_DOMAIN', '*');
        return Socialite::driver($provider)->with(['hd' => $hd])->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        // double check that they are from approved domain, if one is set.

        $allowedDomain = env('SOCIALITE_ALLOW_ONLY_DOMAIN', '');

        if (!empty($allowedDomain)) {
            switch ($provider) {
                case 'google':
                case 'azure':
                    $emailField = 'email';
                    break;
                default:
                    $emailField = '';
                    break;
            }

            $emailAddress = $user->$emailField;
            $emailBits = explode("@", $emailAddress);

            $domain = $emailBits[1]; // TODO - when we get to PHP 7, use array destructuring here.

            if ($domain != $allowedDomain) {
                flash("Error: this email domain is not permitted by this application.", 'danger');
                return redirect("/");
            }

        }


        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);
        return redirect($this->redirectTo);
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('id_from_provider', $user->id)->first();

        if ($authUser) {
            return $authUser;
        }
        return User::create([
            'first_name'     => $user->user["name"]["givenName"],
            'last_name'     => $user->user["name"]["familyName"],
            'email'    => $user->email,
            'provider' => $provider,
            'id_from_provider' => $user->id,
            'avatar_url' => $user->avatar_original,
            'time_zone' => "America/New_York"
        ]);
    }
}
