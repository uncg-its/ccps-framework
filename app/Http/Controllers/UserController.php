<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Permission;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller {

    public function __construct() {
        $this->middleware('permission:users-view')->only(['index','show']);
        $this->middleware('permission:users-edit')->only(['edit','update']);
        $this->middleware('permission:users-delete')->only('destroy');
        $this->middleware('permission:users-create')->only(['create','store']);
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index() {
        $users = User::all();
        return view('admin.users.index')->with(compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create() {
        $roles = Role::all();
        return view('admin.users.create')->with(compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     */
    public function store(UserCreateRequest $request) {
        $user = $request->persist();

        return redirect('/admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  User $user
     */
    public function show(User $userToShow) {
        $userToShow->load(['roles', 'permissions']);
        return view('admin.users.show')->with(compact('userToShow'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User $user
     */
    public function edit(User $userToEdit) {
        $roles = Role::all();
        return view('admin.users.edit')->with(compact(['userToEdit', 'roles']));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  User $user
     */
    public function update(UserUpdateRequest $request, User $user) {
        $user = $request->persist($user);

        return redirect('/admin/user/' . $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $user
     */
    public function destroy(User $user) {
        if ($user->delete()) {
            flash("User '$user->display_name' deleted successfully.", 'success');
        } else {
            flash("User '$user->display_name' could not be deleted.", 'danger');
        }

        return redirect('/admin/users');
    }
}
