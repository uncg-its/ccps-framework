<?php

namespace App\Http\Controllers;

use App\DbConfig;
use App\Http\Requests\ConfigUpdateRequest;
use Illuminate\Http\Request;

class DbConfigController extends Controller
{
    public function __construct() {
        $this->middleware('permission:config-view')->only('index');
        $this->middleware('permission:config-edit')->only('save');
    }

    public function index() {
        return view('admin.config.index')->with(['dbConfig' => app('dbConfig')]);
    }

    public function save(ConfigUpdateRequest $request) {
        $request->persist();
        return redirect('/admin/config');
    }
}
