<?php

namespace App\Http\Controllers;

use App\Http\Requests\PermissionCreateRequest;
use App\Http\Requests\PermissionUpdateRequest;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;

class PermissionController extends Controller {

    public function __construct() {
        $this->middleware('permission:acl-view')->only(['index','show']);
        $this->middleware('permission:acl-edit')->only(['edit','update']);
        $this->middleware('permission:acl-delete')->only('destroy');
        $this->middleware('permission:acl-create')->only(['create','store']);
    }

    /**
     * Display a listing of the resource.
     */
    public function index() {
        $permissions = Permission::all();
        return view('acl.permissions.index')->with(compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create() {
        $roles = Role::all();
        return view('acl.permissions.create')->with(array_merge(compact('roles'), ["permission" => new Permission]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PermissionCreateRequest $request
     */
    public function store(PermissionCreateRequest $request) {
        $permission = $request->persist();
        return redirect('/admin/acl/permissions/');
    }

    /**
     * Display the specified resource.
     *
     * @param  Permission $permission
     */
    public function show(Permission $permission) {
        $permission->load('roles');
        return view('acl.permissions.show')->with(compact('permission'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Permission $permission
     */
    public function edit(Permission $permission) {
        $roles = Role::all();
        return view('acl.permissions.edit')->with(compact(['permission', 'roles']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PermissionUpdateRequest $request
     * @param  Permission $permission
     */
    public function update(PermissionUpdateRequest $request, Permission $permission) {
        $result = $request->persist($permission);
        return redirect('/admin/acl/permission/' . $permission->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Permission $permission
     */
    public function destroy(Permission $permission) {
        if ($permission->delete()) {
            flash("Permission '$permission->display_name' deleted successfully.", 'success');
        } else {
            flash("Permission '$permission->display_name' could not be deleted.", 'danger');
        }

        return redirect('/admin/acl/permissions');
    }
}
