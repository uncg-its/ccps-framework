<?php

namespace App\Http\Requests;

use App\Http\Requests\FlashedRequest;
use App\Role;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class UserCreateRequest extends FlashedRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email'
        ];
    }

    /**
     * Actions to take after successful validation.
     */
    public function persist() {

        DB::beginTransaction();

        try {
            $user = User::create(
                array_merge($this->only(['first_name', 'last_name', 'email']), [
                    'time_zone' => config('app.timezone'),
                ])
            );
            $user->roles()->sync($this->roles);

            DB::commit();
            flash("Successfully created User with email '$this->email'", 'success');
            return $user;

        } catch (\Exception $e) {
            DB::rollBack();
            flash('Database error trying to create User: ' . $e->getMessage(), 'danger');
            return false;
        }
    }
}
