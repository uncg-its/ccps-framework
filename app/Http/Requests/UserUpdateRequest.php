<?php

namespace App\Http\Requests;

use App\Http\Requests\FlashedRequest;
use App\Permission;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class UserUpdateRequest extends FlashedRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'email'
        ];
    }

    /**
     * Actions to take after successful validation.
     */
    public function persist(User $user) {

        // for related roles
        DB::beginTransaction();

        try {

            $updateArray = $this->only(['first_name', 'last_name']);
            if (!is_null($this->email)) {
                $updateArray['email'] = $this->email;
            }

            $user->update($updateArray);
            $user->roles()->sync($this->roles);

            DB::commit();
            flash("Successfully edited user '$user->email'", 'success');
            return $user;

        } catch (\Exception $e) {
            DB::rollBack();
            flash('Database error trying to edit User: ' . $e->getMessage(), 'danger');
            return false;
        }
    }
}
