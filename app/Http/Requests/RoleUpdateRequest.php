<?php

namespace App\Http\Requests;

use App\Http\Requests\FlashedRequest;
use App\Permission;
use App\Role;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class RoleUpdateRequest extends FlashedRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'display_name' => 'required',
        ];
    }

    /**
     * Actions to take after successful validation.
     */
    public function persist(Role $role) {

        DB::beginTransaction();
        try {
            $role->update(
                array_merge($this->only(['display_name', 'description']), ['name' => str_slug($this->display_name)])
            );

            $role->permissions()->sync($this->permissions);

            DB::commit();
            flash("Successfully edited role '$this->display_name'", 'success');
            return $role;

        } catch (\Exception $e) {
            DB::rollBack();
            flash('Database error trying to edit Role: ' . $e->getMessage(), 'danger');
            return false;
        }
    }
}
