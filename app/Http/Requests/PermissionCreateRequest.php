<?php

namespace App\Http\Requests;

use App\Http\Requests\FlashedRequest;
use App\Permission;
use App\Role;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class PermissionCreateRequest extends FlashedRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'display_name' => 'required',
        ];
    }

    /**
     * Actions to take after successful validation.
     */
    public function persist() {

        DB::beginTransaction();
        try {
            $permission = Permission::create(
                array_merge($this->only(['display_name', 'description']), ['name' => str_slug($this->display_name)])
            );

            $permission->roles()->sync($this->roles);

            DB::commit();
            flash("Successfully created Permission '$this->display_name'", 'success');
            return $permission;

        } catch (\Exception $e) {
            DB::rollBack();
            flash('Database error trying to create Permission: ' . $e->getMessage(), 'danger');
            return false;
        }
    }
}
