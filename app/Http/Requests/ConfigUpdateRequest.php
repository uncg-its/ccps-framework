<?php

namespace App\Http\Requests;

use App\DbConfig;
use App\Http\Requests\FlashedRequest;
use App\Permission;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class ConfigUpdateRequest extends FlashedRequest
{
    protected $nonDataFields = ["_token" => "csrf token", "_method" => "custom method"];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Additional validation rules for this object only (define global validation rules in ValidatorServiceProvider)
     *
     *
     */

    public function customValidation($validator) {

        /* punting on this for now. TODO: keep, or remove?
        $inputsToCheck = array_diff_key($this->input(), $this->nonDataFields);

        $dbConfig = DbConfig::all()->keyBy('key');

//        $inputsToCheck["testing"] = "bad data";

        foreach($inputsToCheck as $key => $value) {
            if (!$dbConfig->has($key)) {
                $validator->errors()->add($key, "Configuration item '$key' does not currently exist in the config table. Value cannot be updated.");
            }
        }

//        return $validator;
        */
    }

    /**
     * Actions to take after successful validation.
     */
    public function persist() {

        // we know all keys exist in database.
        $inputsToUpdate = array_diff_key($this->input(), $this->nonDataFields);

        DB::beginTransaction();

        try {
            foreach ($inputsToUpdate as $key => $value) {
                DbConfig::updateOrCreate(['key' => $key], ['value' => $value]);

//                DbConfig::where('key', $key)->update(['value' => $value]);
            }

            DB::commit();
            flash("Successfully edited app configuration.", 'success');
            return true;

        } catch (\Exception $e) {
            DB::rollBack();
            flash('Database error trying to edit app configuration: ' . $e->getMessage(), 'danger');
            return false;
        }
    }
}
