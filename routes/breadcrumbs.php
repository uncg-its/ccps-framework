<?php

// HOME
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push("Home", url('/'));
});

// ADMIN
Breadcrumbs::register('admin', function ($breadcrumbs) {
    $breadcrumbs->push("Admin", url('/admin'));
});

Breadcrumbs::register('users', function ($breadcrumbs) {
    $breadcrumbs->parent('admin');
    $breadcrumbs->push("Users", url('/admin/users'));
});
Breadcrumbs::register('acl', function ($breadcrumbs) {
    $breadcrumbs->parent('admin');
    $breadcrumbs->push("ACL", url('/admin/acl'));
});
Breadcrumbs::register('config', function ($breadcrumbs) {
    $breadcrumbs->parent('admin');
    $breadcrumbs->push("Config", url('/admin/config'));
});

// USERS

Breadcrumbs::register('user-create', function ($breadcrumbs) {
    $breadcrumbs->parent('users');
    $breadcrumbs->push("New User", url('/admin/acl/user/create'));
});
Breadcrumbs::register('user-show', function ($breadcrumbs, $userToShow) {
    $breadcrumbs->parent('users');
    $breadcrumbs->push($userToShow->email, url('/admin/user/' . $userToShow->id));
});
Breadcrumbs::register('user-edit', function ($breadcrumbs, $userToEdit) {
    $breadcrumbs->parent('users');
    $breadcrumbs->push($userToEdit->email, url('/admin/user/' . $userToEdit->id));
    $breadcrumbs->push("Edit", url('/admin/user/' . $userToEdit->id . '/edit'));
});
// ... continue with CRUD ...

// ROLES
Breadcrumbs::register('roles', function ($breadcrumbs) {
    $breadcrumbs->parent('acl');
    $breadcrumbs->push("Roles", url('/admin/acl/roles'));
});
Breadcrumbs::register('role-create', function ($breadcrumbs) {
    $breadcrumbs->parent('roles');
    $breadcrumbs->push("New Role", url('/admin/acl/role/create'));
});
Breadcrumbs::register('role-show', function ($breadcrumbs, $role) {
    $breadcrumbs->parent('roles');
    $breadcrumbs->push($role->display_name, url('/admin/acl/role/' . $role->id));
});
Breadcrumbs::register('role-edit', function ($breadcrumbs, $role) {
    $breadcrumbs->parent('roles');
    $breadcrumbs->push($role->display_name, url('/admin/acl/role/' . $role->id));
    $breadcrumbs->push("Edit", url('/admin/acl/role/' . $role->id . '/edit'));
});

// PERMISSIONS
Breadcrumbs::register('permissions', function ($breadcrumbs) {
    $breadcrumbs->parent('acl');
    $breadcrumbs->push("Permissions", url('/admin/acl/permissions'));
});
Breadcrumbs::register('permission-create', function ($breadcrumbs) {
    $breadcrumbs->parent('permissions');
    $breadcrumbs->push("New Permission", url('/admin/acl/permission/create'));
});
Breadcrumbs::register('permission-show', function ($breadcrumbs, $permission) {
    $breadcrumbs->parent('permissions');
    $breadcrumbs->push($permission->display_name, url('/admin/acl/permission/' . $permission->id));
});
Breadcrumbs::register('permission-edit', function ($breadcrumbs, $permission) {
    $breadcrumbs->parent('permissions');
    $breadcrumbs->push($permission->display_name, url('/admin/acl/permission/' . $permission->id));
    $breadcrumbs->push("Edit", url('/admin/acl/permission/' . $permission->id . '/edit'));
});


// ACCOUNT
Breadcrumbs::register('account', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push("Account", url('/account'));
});
Breadcrumbs::register('profile-show', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push("My Profile", url('/account/profile'));
});
Breadcrumbs::register('profile-edit', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push("Edit Profile", url('/account/profile/edit'));
});
Breadcrumbs::register('account-settings', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push("Account Settings", url('/account/settings'));
});



// DEMO - can remove eventually
Breadcrumbs::register('demo', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push("Demo", url('/demo'));
});

Breadcrumbs::register('breadcrumbs', function ($breadcrumbs) {
    $breadcrumbs->parent('demo');
    $breadcrumbs->push("Breadcrumbs Demo", url('/demo/breadcrumbs'));
});