<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@index');

Route::group(['prefix' => 'demo'], function() {
    Route::get('/', 'DemoController@index');

    Route::get('themes', 'DemoController@themes'); // all themes
    Route::post('themes', 'DemoController@setTheme'); // set theme in session
    Route::get('theme/{themeName}', 'DemoController@theme'); // specific theme

    Route::get('flash', 'DemoController@flash');
    Route::post('flash', 'DemoController@flashMessages');

    Route::get('breadcrumbs', 'DemoController@breadcrumbs');

    Route::get('guzzle', 'DemoController@guzzle');

    Route::get('menu', 'DemoController@menu');

    Route::get('email', 'DemoController@email');
    Route::post('email', 'DemoController@sendEmail');
});

Route::group(['prefix' => 'account'], function() {
    Route::get('/', 'AccountController@index');
    Route::get('/profile', 'AccountController@profile');
    Route::get('/profile/edit', 'AccountController@editProfile');
    Route::get('/settings', 'AccountController@settings');
});

Route::group(['prefix' => 'admin'], function() {
    Route::get('/', 'AdminController@index');

    Route::get('users', 'UserController@index');
    Route::get('users/new', 'UserController@create');
    Route::post('users/new', 'UserController@store');
    Route::get('user/{userToShow}', 'UserController@show');
    Route::get('user/{userToEdit}/edit', 'UserController@edit');
    Route::patch('user/{user}', 'UserController@update');
    Route::delete('user/{user}', 'UserController@destroy');

    Route::group(['prefix' => 'acl'], function() {
        Route::get('/', 'AclController@index');

        Route::get('roles', 'RoleController@index');
        Route::get('roles/new', 'RoleController@create');
        Route::post('roles/new', 'RoleController@store');
        Route::get('role/{role}', 'RoleController@show');
        Route::get('role/{role}/edit', 'RoleController@edit');
        Route::patch('role/{role}', 'RoleController@update');
        Route::delete('role/{role}', 'RoleController@destroy');

        Route::get('permissions', 'PermissionController@index');
        Route::get('permissions/new', 'PermissionController@create');
        Route::post('permissions/new', 'PermissionController@store');
        Route::get('permission/{permission}', 'PermissionController@show');
        Route::get('permission/{permission}/edit', 'PermissionController@edit');
        Route::patch('permission/{permission}', 'PermissionController@update');
        Route::delete('permission/{permission}', 'PermissionController@destroy');
    });

    Route::get('config', 'DbConfigController@index');
    Route::patch('config', 'DbConfigController@save');
});

// For local auth
Auth::routes();
// Socialite - User Account OAuth Routes
Route::get('auth/{provider}', 'AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'AuthController@handleProviderCallback');
