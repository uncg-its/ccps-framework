const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/themes/base/assets/js/base.js', 'public/base/js')
   .sass('resources/themes/base/assets/sass/base.scss', 'public/base/css')

    .js('resources/themes/admin-lte/assets/js/admin-lte.js', 'public/admin-lte/js')
    .sass('resources/themes/admin-lte/assets/sass/admin-lte.scss', 'public/admin-lte/css')

    .js('resources/themes/uncg/assets/js/uncg.js', 'public/uncg/js')
    .sass('resources/themes/uncg/assets/sass/uncg.scss', 'public/uncg/css')

    .version();
