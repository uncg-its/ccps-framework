@extends('layouts.wrapper', [
    'pageTitle' => '403'
])

@section('content')
    <h2>403 - Forbidden</h2>
    <p>{{ $exception->getMessage() }}</p>
@endsection()