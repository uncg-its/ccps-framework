@if($permission->editable)
    <div class="form-group {{ empty($errors->get('display_name')) ?: " has-error" }}">
        <label for="display_name">Name:*</label>
        <input type="text" class="form-control" id="display_name" name="display_name" placeholder="Name" value="{{ old('display_name', $permission->display_name) }}">
        <p class="help-block"><em>Database name</em>: <span id="name"></span></p>
    </div>
    <div class="form-group">
        <label for="description">Description:</label>
        <input type="text" class="form-control" id="description" name="description" placeholder="Description" value="{{ old('description', $permission->description) }}">
    </div>
@else
    <div class="alert alert-warning">
        <i class="fa fa-exclamation-triangle"></i> <strong>Notice</strong>: This permission's display information cannot be changed.
    </div>
@endif

@if(count($roles) > 0)
    <p><strong>Apply this permission to roles:</strong></p>
    @foreach($roles as $role)
        <div class="checkbox">
            <label>
                <input type="checkbox" value="{{ $role->id }}" name="roles[]"
                @if(old('roles') !== null)
                    @if(in_array($role->id, old('roles')))
                        checked
                    @endif
                @elseif ($permission->roles->contains($role->id))
                    checked
                @endif
                >
                {{ $role->display_name }}
            </label>
        </div>
    @endforeach
@endif