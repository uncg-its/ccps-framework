@if($role->editable)
    <div class="form-group {{ empty($errors->get('display_name')) ?: " has-error" }}">
        <label for="display_name">Name:*</label>
        <input type="text" class="form-control" id="display_name" name="display_name" placeholder="Name" value="{{ old('display_name', $role->display_name) }}">
        <p class="help-block"><em>Database name</em>: <span id="name"></span></p>
    </div>
    <div class="form-group">
        <label for="description">Description:</label>
        <input type="text" class="form-control" id="description" name="description" placeholder="Description" value="{{ old('description', $role->description) }}">
    </div>
@else
    <div class="alert alert-warning">
        <i class="fa fa-exclamation-triangle"></i> <strong>Notice</strong>: This role's display information cannot be changed.
    </div>
@endif

@if(count($permissions) > 0)
    <p><strong>Permissions to apply to this role:</strong></p>
    @foreach($permissions as $permission)
        <div class="checkbox">
            <label>
                <input type="checkbox" value="{{ $permission->id }}" name="permissions[]"
                @if(old('permissions') !== null)
                    @if(in_array($permission->id, old('permissions')))
                        checked
                    @endif
                @elseif ($role->permissions->contains($permission->id))
                    checked
                @endif
                >
                {{ $permission->display_name }}
            </label>
        </div>
    @endforeach
@endif