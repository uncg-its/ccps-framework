@extends('layouts.wrapper', [
    'pageTitle' => 'Theme Demo'
])

@section('content')
    <h1>Theme Demo</h1>
    <p>Theme can be controlled either in: controller, method, or session (or middleware...but that's too fancy for us today :) )</p>

    <form action="/demo/themes" method="post">
        <label> Set with the session:
            <select class="form-group" name="theme">
                <option value="">--- Select ---</option>
                <option value="base">base</option>
                <option value="uncg">uncg</option>
                <option value="admin-lte">admin-lte</option>
                <option value="clear">clear session (revert to default)</option>
            </select>
        </label>

        {{ csrf_field() }}

        <button type="submit" class="btn btn-success">Set theme in session</button>
    </form>

@endsection