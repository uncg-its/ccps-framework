@extends('layouts.wrapper', [
    'pageTitle' => 'Admin Home'
])

@section('content')
    {!! Breadcrumbs::render('admin') !!}

    <h1>Admin Home</h1>

    <div class="row">
        @include('components.panel-nav', [
            'url' => '/admin/users',
            'fa' => 'users',
            'title' => 'Users'
        ])
        @include('components.panel-nav', [
            'url' => '/admin/acl',
            'fa' => 'list',
            'title' => 'ACL'
        ])
        @include('components.panel-nav', [
            'url' => '/admin/config',
            'fa' => 'cog',
            'title' => 'App Config'
        ])
    </div>
@endsection()