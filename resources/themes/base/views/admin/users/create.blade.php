@extends('layouts.wrapper', [
    'pageTitle' => 'Users - Add New'
])

@section('content')
    {!! Breadcrumbs::render('user-create') !!}

    <h1>Add New User</h1>

    <form action="/admin/users/new" method="POST">
        <div class="form-group {{ empty($errors->get('first_name')) ?: " has-error" }}">
            <label for="first_name">First Name:*</label>
            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="{{ old('first_name') }}">
        </div>
        <div class="form-group {{ empty($errors->get('last_name')) ?: " has-error" }}">
            <label for="last_name">Last Name:*</label>
            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="{{ old('last_name') }}">
        </div>

        <div class="form-group {{ empty($errors->get('email')) ?: " has-error" }}">
            <label for="email">Email address:*</label>
            <input type="text" class="form-control" id="email" name="email" placeholder="Name" value="{{ old('email') }}">
        </div>


        @if(count($roles) > 0)
            <p><strong>Roles to apply to this user:</strong></p>
            @foreach($roles as $role)
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="{{ $role->id }}" name="roles[]"
                            @if(old('roles') !== null)
                                @if(in_array($role->id, old('roles')))
                                checked
                                @endif
                            @endif
                        >
                        {{ $role->display_name }}
                    </label>
                </div>
            @endforeach
        @endif

        {{ csrf_field() }}

        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Add</button>
        <a href="/admin/users" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
    </form>
@endsection()
