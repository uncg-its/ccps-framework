@extends('layouts.wrapper', [
    'pageTitle' => 'Users - index'
])

@section('content')
    {!! Breadcrumbs::render('users') !!}

    <h1>Users Manager</h1>
    @if(count($users) > 0)
        <ul class="list-group">
            @foreach($users as $user)
                <li class="list-group-item">
                    <b>{{ $user->first_name}} {{ $user->last_name }} ({{ $user->email }})</b>
                    <div class="pull-right">
                        <form method="POST" action="/admin/user/{{ $user->id }}" onsubmit="return confirm('Do you really want to delete this user?')">
                            <a href="/admin/user/{{ $user->id }}" class="btn btn-primary"><i class="fa fa-list"></i> Details</a>

                            @permission('users-edit')
                                <a href="/admin/user/{{ $user->id }}/edit" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                            @endpermission

                            @permission('users-delete')
                                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                            @endpermission

                            {{ csrf_field() }}
                            {{ method_field("DELETE") }}
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </li>
            @endforeach
        </ul>
    @else
        <p>No users exist.</p>
    @endif

    @permission('users-create')
        <a href="/admin/users/new" class="btn btn-success"><i class="fa fa-plus"></i> Add New</a>
    @endpermission
@endsection