@extends('layouts.wrapper', [
    'pageTitle' => 'Configuration - Index'
])

@section('content')
    {!! Breadcrumbs::render('config') !!}

    <h1>App Configuration</h1>

    @nopermission('config-edit')
        <div class="alert alert-warning">
            <i class="fa fa-exclamation-triangle"></i> <strong>Notice</strong>: you do not have sufficient permissions to edit application configuration variables.
        </div>
    @endnopermission

    <form action="/admin/config" method="POST">
        {{-- Base App configuration --}}

        <div class="form-group">
            <label for="theme">Default Theme:</label>
            <select id="theme" class="form-control" name="theme"
                @nopermission('config-edit')
                    disabled
                @endnopermission
            >
                <option value="">--- Select ---</option>
                @foreach(config('themes.themes') as $key => $theme)
                    <option value="{{ $key }}"
                        @if(old('theme') == $key || (isset($dbConfig['theme']) && $dbConfig['theme'] == $key))
                            selected
                        @endif
                    >
                        {{ $theme["displayName"] }} ({{ $key }})
                    </option>
                @endforeach
            </select>
            <p class="help-block">This is the default theme for all pages (can still be overridden in controller)</p>
        </div>

        <div class="form-group">
            <label for="debugBar">Show Debug Bar?</label>
            <select id="debugBar" class="form-control" name="debugBar"
                @nopermission('config-edit')
                    disabled
                @endnopermission
            >
                <option value="enabled"
                    @if(old('debugBar') == 'enabled' || (isset($dbConfig['debugBar']) && $dbConfig['debugBar'] == 'enabled'))
                        selected
                    @endif
                >
                    Yes
                </option>
                <option value="disabled"
                    @if(old('debugBar') == 'disabled' || (isset($dbConfig['debugBar']) && $dbConfig['debugBar'] == 'disabled'))
                        selected
                    @endif
                >
                    No
                </option>
            </select>
            <p class="help-block">If enabled, and app is not in production mode, the Debug Bar will be shown</p>
        </div>
    
    
        {{-- Custom App configuration --}}


        
        {{-- Submit --}}
        {{ csrf_field() }}
        {{ method_field("PATCH") }}

        @permission('config-edit')
            <button type="submit" class="btn btn-success">Update Configuration</button>
        @endpermission
        
    </form>
@endsection()