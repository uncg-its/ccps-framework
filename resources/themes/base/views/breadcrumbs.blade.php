@extends('layouts.wrapper', [
    'pageTitle' => 'Breadcrumbs demo'
])

@section('content')

    {!! Breadcrumbs::render('breadcrumbs') !!}

    <h1>Breadcrumbs Demo</h1>
    <p>Defining the breadcrumbs and rendering them is pretty easy. Look at the above!</p>

@endsection