@extends('layouts.wrapper', [
    'pageTitle' => 'Index'
])

@section('content')
    <h1>CCPS Framework Demo Site</h1>
    <ul>
        <li><a href="/demo/themes">Themes demo page</a></li>
        <li><a href="/demo/theme/uncg">UNCG Theme demo page</a></li>
        <li><a href="/demo/flash">Flash messages demo</a></li>
        <li><a href="/demo/breadcrumbs">Breadcrumbs demo</a></li>
        <li><a href="/demo/guzzle">Guzzle (HTTP Requests) demo</a></li>
        <li><a href="/demo/menu">Nav menu demo</a></li>
    </ul>

    <p>Auth</p>
    <ul>
        @if (!Auth::check())
            <li><a href="/login">Log In</a></li>
            <li><a href="/register">Register</a></li>
        @else
            <li><a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                    Log Out
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form></li>
        @endif
    </ul>
@endsection