@extends('layouts.wrapper', [
    'pageTitle' => 'Email Demo'
])

@section('content')
    @if(count($errors))
        <div class="alert alert-danger">
            <p>Please correct the following errors:</p>
            <ul>
            @foreach($errors->all() as $index => $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif

    <h1>Email Demo</h1>

    <p>This is a page where you're allowed to send an email. Aren't you lucky?</p>

    <form method="post" action="/demo/email">
        <div class="form-group">
            <label for="to">To:</label>
            <input type="text" class="form-control" id="to" name="to" placeholder="example@domain.com">
            <p class="help-block">Address to send the message to</p>
        </div>
        <div class="form-group">
            <label for="message">Message:</label>
            <textarea class="form-control" id="message" name="message" placeholder="Hello there."></textarea>
        </div>
        
        {{ csrf_field() }}
        
        <button type="submit" class="btn btn-primary"><i class="fa fa-envelope"></i> Send the email!</button>
    </form>
@endsection