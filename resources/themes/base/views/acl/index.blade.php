@extends('layouts.wrapper', [
    'pageTitle' => 'ACL - Index'
])

@section('content')
    {!! Breadcrumbs::render('acl') !!}

    <h1>ACL</h1>

    <div class="row">
        @include('components.panel-nav', [
            'url' => '/admin/acl/roles',
            'fa' => 'id-badge',
            'title' => 'Roles'
        ])
        @include('components.panel-nav', [
            'url' => '/admin/acl/permissions',
            'fa' => 'lock',
            'title' => 'Permissions'
        ])
    </div>
@endsection()