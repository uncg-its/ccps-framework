@extends('layouts.wrapper', [
    'pageTitle' => 'Permissions - Add New'
])

@section('content')
    {!! Breadcrumbs::render('permission-create') !!}

    <h1>Add New Permission</h1>

    <form action="/admin/acl/permissions/new" method="POST">
        @include('forms.permission')

        {{ csrf_field() }}

        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
        <a href="/admin/acl/permissions" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
    </form>
@endsection()

@section('scripts')
    <script>
        $(function() {
            var $name = $("#name");
            var $display_name = $("#display_name");

            $display_name.change(function() {
                $name.html(str_slug($display_name.val()));
            });
        });
    </script>
@endsection
