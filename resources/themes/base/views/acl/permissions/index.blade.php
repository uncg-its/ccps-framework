@extends('layouts.wrapper', [
    'pageTitle' => 'Permissions - index'
])

@section('content')
    {!! Breadcrumbs::render('permissions') !!}

    <h1>Permissions Manager</h1>
    @if(count($permissions) > 0)
        <ul class="list-group">
            @foreach($permissions as $permission)
                <li class="list-group-item">
                    <b>{{ $permission->display_name or $permission->name}}</b>
                    <div class="pull-right">
                        <form method="POST" action="/admin/acl/permission/{{ $permission->id }}" onsubmit="return confirm('Do you really want to delete this permission?')">
                            <a href="/admin/acl/permission/{{ $permission->id }}" class="btn btn-primary"><i class="fa fa-list"></i> Details</a>

                            @permission('acl-edit')
                                <a href="/admin/acl/permission/{{ $permission->id }}/edit" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                            @endpermission

                            @permission('acl-delete')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            @endpermission

                            {{ csrf_field() }}
                            {{ method_field("DELETE") }}
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </li>
            @endforeach
        </ul>
    @else
        <p>No permissions exist.</p>
    @endif
    @permission('acl-create')
        <a href="/admin/acl/permissions/new" class="btn btn-success"><i class="fa fa-plus"></i> Add New</a>
    @endpermission
@endsection