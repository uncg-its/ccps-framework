@extends('layouts.wrapper', [
    'pageTitle' => 'Permission - ' . $permission->name
])

@section('content')
    {!! Breadcrumbs::render('permission-show', $permission) !!}

    <h1>Permission - {{ $permission->name }}</h1>

    <p>Key Name: {{ $permission->name }}</p>
    @isset($permission->display_name)
        <p>Display Name: {{ $permission->display_name }}</p>
    @endisset
    @isset($permission->description)
        <p>Description: {{ $permission->description }}</p>
    @endisset

    <form method="POST" action="/admin/acl/role/{{ $permission->id }}" onsubmit="return confirm('Do you really want to delete this permission?')">

        @permission('acl-edit')
            <a href="/admin/acl/permission/{{ $permission->id }}/edit" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
        @endpermission

        @permission('acl-delete')
            <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
        @endpermission

        {{ csrf_field() }}
        {{ method_field("DELETE") }}
    </form>

    <h2>Roles to which this permission belongs</h2>
    @if(count($permission->roles) > 0)
        <ul>
            @foreach($permission->roles as $index => $permission)
                <li>{{ $permission->display_name }}</li>
            @endforeach
        </ul>
    @else
        <p>No roles.</p>
    @endif


@endsection()