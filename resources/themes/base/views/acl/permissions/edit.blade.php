@extends('layouts.wrapper', [
    'pageTitle' => 'Roles - Edit'
])

@section('content')
    {!! Breadcrumbs::render('permission-edit', $permission) !!}

    <h1>Edit Permission - {{ $permission->display_name }}</h1>

    <form action="/admin/acl/permission/{{ $permission->id }}" method="POST">
        @include('forms.permission')

        {{ csrf_field() }}
        {{ method_field("PATCH") }}

        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
        <a href="{{ URL::previous() }}" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
    </form>
@endsection()

@section('scripts')
    <script>
        $(function() {
            var $name = $("#name");
            var $display_name = $("#display_name");

            $name.html(str_slug($display_name.val()));

            $display_name.change(function() {
                $name.html(str_slug($display_name.val()));
            });
        });
    </script>
@endsection
