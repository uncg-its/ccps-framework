@extends('layouts.wrapper', [
    'pageTitle' => 'Roles - index'
])

@section('content')

    {!! Breadcrumbs::render('roles') !!}

    <h1>Roles Manager</h1>
    @if(count($roles) > 0)
        <ul class="list-group">
            @foreach($roles as $role)
                <li class="list-group-item">
                    <b>{{ $role->display_name or $role->name}}</b>
                    <div class="pull-right">
                        <form method="POST" action="/admin/acl/role/{{ $role->id }}" onsubmit="return confirm('Do you really want to delete this role?')">

                            <a href="/admin/acl/role/{{ $role->id }}" class="btn btn-primary"><i class="fa fa-list"></i> Details</a>

                            @permission('acl-edit')
                            <a href="/admin/acl/role/{{ $role->id }}/edit" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                            @endpermission

                            @permission('acl-delete')
                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                            @endpermission

                            {{ csrf_field() }}
                            {{ method_field("DELETE") }}
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </li>
            @endforeach
        </ul>
    @else
        <p>No roles exist.</p>
    @endif

    @permission('acl-create')
    <a href="/admin/acl/roles/new" class="btn btn-success"><i class="fa fa-plus"></i> Add New</a>
    @endpermission

@endsection