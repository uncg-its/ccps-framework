@extends('layouts.wrapper', [
    'pageTitle' => 'Roles - Add New'
])

@section('content')
    {!! Breadcrumbs::render('role-create') !!}

    <h1>Add New Role</h1>

    <form action="/admin/acl/roles/new" method="POST">
        <div class="form-group {{ empty($errors->get('display_name')) ?: " has-error" }}">
            <label for="display_name">Name:*</label>
            <input type="text" class="form-control" id="display_name" name="display_name" placeholder="Name" value="{{ old('display_name') }}">
            <p class="help-block"><em>Database name</em>: <span id="name"></span></p>
        </div>
        <div class="form-group">
            <label for="description">Description:</label>
            <input type="text" class="form-control" id="description" name="description" placeholder="Description" value="{{ old('description') }}">
        </div>

        @if(count($permissions) > 0)
            <p><strong>Permissions to apply to this role:</strong></p>
            @foreach($permissions as $permission)
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="{{ $permission->id }}" name="permissions[]"
                        @if(old('permissions') !== null)
                            @if(in_array($permission->id, old('permissions')))
                                checked
                            @endif
                        @endif
                        >
                        {{ $permission->display_name }}
                    </label>
                </div>
            @endforeach
        @endif

        {{ csrf_field() }}

        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Add</button>
        <a href="/admin/acl/roles" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
    </form>
@endsection()

@section('scripts')
    <script>
        $(function() {
            var $name = $("#name");
            var $display_name = $("#display_name");

            $display_name.change(function() {
                $name.html(str_slug($display_name.val()));
            });
        });
    </script>
@endsection
