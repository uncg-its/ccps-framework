@extends('layouts.wrapper', [
    'pageTitle' => 'Roles - ' . $role->name
])

@section('content')
    {!! Breadcrumbs::render('role-show', $role) !!}

    <h1>Role - {{ $role->name }}</h1>

    <p>Key Name: {{ $role->name }}</p>
    @isset($role->display_name)
        <p>Display Name: {{ $role->display_name }}</p>
    @endisset
    @isset($role->description)
        <p>Description: {{ $role->description }}</p>
    @endisset

    <form method="POST" action="/admin/acl/role/{{ $role->id }}" onsubmit="return confirm('Do you really want to delete this role?')">

        @permission('acl-edit')
            <a href="/admin/acl/role/{{ $role->id }}/edit" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
        @endpermission

        @permission('acl-delete')
            <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
        @endpermission

        {{ csrf_field() }}
        {{ method_field("DELETE") }}
    </form>

    <h2>Permissions for this role</h2>
    @if(count($role->permissions) > 0)
        <ul>
            @foreach($role->permissions as $index => $permission)
                <li>{{ $permission->display_name }}</li>
            @endforeach
        </ul>
    @else
        <p>No permissions are currently assigned to this role.</p>
    @endif

    <h2>Users currently assigned this role</h2>
    @if(count($role->users) > 0)
        <ul>
            @foreach($role->users as $index => $user)
                <li>{{ $user->first_name . " " . $user->last_name}} ({{ $user->email }})</li>
            @endforeach
        </ul>
    @else
        <p>No users currently hold this role.</p>
    @endif


@endsection()