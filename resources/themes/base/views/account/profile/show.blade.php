@extends('layouts.wrapper', [
    'pageTitle' => 'My Profile'
])

@section('content')
    {!! Breadcrumbs::render('profile-show') !!}

    <h1>User - {{ $userToShow->email }}</h1>

    <div class="row">
        <div class="col-md-2">
            <img class="img-circle img-responsive" src="{{ $userToShow->avatar_url or asset('images/default-avatar.jpg')}}">
        </div>
        <div class="col-md-10">
            <p>Name: {{ $userToShow->first_name }} {{ $userToShow->last_name }}</p>
        </div>
    </div>

    <a href="profile/edit" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>


@endsection()