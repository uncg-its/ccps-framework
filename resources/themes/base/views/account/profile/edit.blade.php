@extends('layouts.wrapper', [
    'pageTitle' => 'Edit Profile'
])

@section('content')
    {!! Breadcrumbs::render('profile-edit') !!}

    <h1>Edit Profile</h1>

    <div class="row">
        <div class="col-md-12"><p>Nothing here yet.</p></div>
    </div>

@endsection()