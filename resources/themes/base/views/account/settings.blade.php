@extends('layouts.wrapper', [
    'pageTitle' => 'Account Settings'
])

@section('content')
    {!! Breadcrumbs::render('account-settings', $userToShow) !!}

    <h1>My Account Settings</h1>

    <div class="row">
        <div class="col-md-12"><p>Nothing here yet.</p></div>
    </div>

@endsection()