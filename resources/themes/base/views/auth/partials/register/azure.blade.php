<div class="panel panel-default">
    <div class="panel-heading">Azure</div>
    <div class="panel-body text-center">
        <a href="{{ url('/auth/azure') }}" class="btn btn-primary btn-success" title="Create account via Azure">
            <i class="fa fa-2x fa-windows"></i> Create account via Azure
        </a>
    </div>
</div>