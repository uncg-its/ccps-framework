@extends('layouts.wrapper', [
    'pageTitle' => 'Dashboard'
])

@section('content')
    <h3>You're logged in as:</h3>
    <p>{{ $user->first_name }} {{ $user->last_name }}</p>
    <ul>
        <li>{{ $user->email }}</li>
        <li>{{ $user->avatar_url }}</li>
    </ul>

    <h3>Logout</h3>
    <a class="btn btn-default" href="{{ route('logout') }}"
       onclick="event.preventDefault();
                 document.getElementById('logout-form').submit();">
        Logout
    </a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
@endsection