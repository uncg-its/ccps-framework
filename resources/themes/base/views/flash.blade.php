@extends('layouts.wrapper', [
    'pageTitle' => 'Flash Message Demo'
])

@section('content')
    <h1>Flash Messages demo</h1>

    <form action="/demo/flash" method="POST">
        <div class="form-group">
            <label for="message">Your Message:</label>
            <input type="text" class="form-control" id="message" name="message" placeholder="Your Message">
        </div>
        <label>
            <select class="form-group" name="type">
                <option value="">--- Select ---</option>
                <option value="success">success</option>
                <option value="info">info</option>
                <option value="warning">warning</option>
                <option value="danger">danger</option>
                <option value="all">one of each!</option>
            </select>
        </label>
        <button type="submit" class="btn btn-default">Submit</button>
        {{ csrf_field() }}
    </form>

@endsection