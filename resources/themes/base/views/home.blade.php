@extends('layouts.wrapper', [
    'pageTitle' => 'Home'
])

@section('content')
    <h1>{{ config('app.name') }}</h1>
    <div class="row">
        @if($user)
            @if($user->hasRole('admin'))
                @include('components.panel-nav', [
                'url' => '/admin',
                'fa' => 'key',
                'title' => 'Administrator'
            ])
            @endif
        @else
            <div class="col-md-12">
                <p>You are not logged in - please <a href="{{ url("/login") }}">log in</a> to proceed.</p>
            </div>
        @endif
    </div>
@endsection