@extends('layouts.wrapper', [
    'pageTitle' => 'Guzzle Demo'
])

@section('content')
    <h1>Menu Demo</h1>

    <p>We'll use the Menu plugin to define our menu in some global middleware, and then render it in the Blade templates for each theme. For now, here's some fun with the menu itself.</p>

    <p>This plugin (<em>lavary/laravel-menu</em>) is extensible so we can Blade-in our nav to our themes. We'll want to do that with nested nav capability.</p>

    <h3>Raw</h3>

    <pre>
        {{ $nav->all() }}
    </pre>

    <h3>Render as &lt;ul&gt;</h3>

    {!! $nav->asUl(['class' => 'myNavClass']) !!}
    <pre>
        {{ $nav->asUl(['class' => 'myNavClass']) }}
    </pre>

    <h3>Render as &lt;ol&gt;</h3>

    {!! $nav->asOl(['class' => 'myNavClass']) !!}
    <pre>
        {{ $nav->asOl(['class' => 'myNavClass']) }}
    </pre>

    <h3>Render as &lt;div&gt;</h3>

    {!! $nav->asDiv(['class' => 'myNavClass']) !!}
    <pre>
        {{ $nav->asDiv(['class' => 'myNavClass']) }}
    </pre>

    <h3>Bootstrap</h3>

    @include('components.nav')


@endsection