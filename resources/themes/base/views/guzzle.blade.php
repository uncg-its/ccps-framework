@extends('layouts.wrapper', [
    'pageTitle' => 'Guzzle Demo'
])

@section('content')
    <h1>Guzzle Demo</h1>

    <p>For now, we'll just show the result.</p>

    <pre>
        {{ print_r($result) }}
    </pre>
@endsection