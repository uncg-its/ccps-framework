@foreach($items as $item)
    @if($item->hasChildren())
        <li class="treeview">
            <a href="{!! $item->url() !!}"><span>{!! $item->title !!}</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                @include('components.menu-item', ['items' => $item->children()])
            </ul>
        </li>
    @else
        <li><a href="{!! $item->url() !!}">{!! $item->title !!}</a></li>
    @endif
@endforeach