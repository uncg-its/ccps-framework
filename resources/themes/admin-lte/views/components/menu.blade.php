<ul class="sidebar-menu">
    @include('components.menu-item', ['items'=> $nav->roots()])

    @if(!$user)
        <li><a href="{{ url('/login') }}">Log In</a></li>
    @endif
</ul><!-- /.sidebar-menu -->