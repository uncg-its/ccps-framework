<!-- Menus -->
<li class="dropdown {{ $type }}-menu">
    <!-- Menu toggle button -->
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        @if ($type == "messages")
            <i class="fa fa-envelope-o"></i>
        @elseif ($type == "notifications")
            <i class="fa fa-exclamation-triangle"></i>
        @elseif ($type == "tasks")
            <i class="fa fa-list-alt"></i>
        @endif

        @if(count($items) > 0)
            <span class="label label-{{ $label }}">{{  count($items) }}</span>
        @endif
    </a>
    <ul class="dropdown-menu">
        <li class="header">You have {{ count($items) }} {{ str_plural($type, count($items)) }}</li>
        <li>
            <!-- inner menu: contains the items -->
            <ul class="menu">
                @if($type == "messages")
                    <li><!-- start item-->
                        <a href="#">
                            <div class="pull-left">
                                <!-- User Image -->
                                <img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image"/>
                            </div>
                            <!-- Message title and timestamp -->
                            <h4>
                                Support Team
                                <small><i class="fa fa-clock-o"></i> 5 mins</small>
                            </h4>
                            <!-- The message -->
                            <p>Why not buy a new awesome theme?</p>
                        </a>
                    </li><!-- end message -->
                @elseif($type == "notifications")
                    <li>
                        <!-- Inner Menu: contains the notifications -->
                        <ul class="menu">
                            <li><!-- start notification -->
                                <a href="#">
                                    <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                </a>
                            </li><!-- end notification -->
                        </ul>
                    </li>
                @elseif($type == "tasks")
                    <li><!-- Task item -->
                        <a href="#">
                            <!-- Task title and progress text -->
                            <h3>
                                Design some buttons
                                <small class="pull-right">20%</small>
                            </h3>
                            <!-- The progress bar -->
                            <div class="progress xs">
                                <!-- Change the css width attribute to simulate progress -->
                                <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                    <span class="sr-only">20% Complete</span>
                                </div>
                            </div>
                        </a>
                    </li><!-- end task item -->
                @endif


            </ul><!-- /.menu -->
        </li>
        <li class="footer"><a href="#">See All {{ ucwords($type) }}</a></li>
    </ul>
</li><!-- /.messages-menu -->