<nav class="navbar navbar-default" id="uncgtheme-navbar">
    <div class="container-fluid">
         {{--Mobile--}}
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed navbar-right" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
             {{--Brand--}}
            <a class="navbar-brand hidden-xs" href="{{ url("/") }}">{{ strtoupper(config('app.name')) }}</a>
            <div class="pull-right home-icon visible-xs">
                <a href="{{ url("/") }}"><i class="fa fa-2x fa-home"></i></a>
            </div>
        </div>

         {{--Desktop--}}
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                @include(config('laravel-menu.views.bootstrap-items'), array('items' => $nav->roots()))
            </ul>

            @if(env('APP_SEARCH'))
                <form class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            @endif

            <div class="navbar-right">
                @if($user)
                    <p class="navbar-text">Logged in as <a href="/account">{{ $user->email }}</a></p>
                @else
                    <p class="navbar-text"><a href="{{ url('/login') }}">Log in</a></p>
                @endif
            </div>
        </div><!-- /.navbar-collapse -->
    </div>
</nav>

