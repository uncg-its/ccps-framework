<?php
//if(defined('UNIT_VARS')) include(UNIT_VARS);  // get unit variables if the variable exists in the page that called the header.

$doc_path = dirname(__FILE__); // file path for this document

?>
        <!DOCTYPE html>
<!-- UNCG WEB3 WRAPPER::HEADER::PHP VERSION 3.0.0::9/1/2014 -->
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ config('uncgtheme.unit_name') ?: "UNCG" }} | {{ $pageTitle }}</title>

    <link rel='stylesheet' id='uncg-libraries'  href='/vendor/uncg/wrapper/rincuncg/css/lib.min.css?ver=1.0.0' type='text/css' media='all' />
    <link rel='stylesheet' id='uncg-print-css'  href='/vendor/uncg/wrapper/rincuncg/css/print.min.css?ver=1.0.0' type='text/css' media='print' />
    <link rel='stylesheet' id='fastfonts-css'  href='//fast.fonts.com/cssapi/7860d9f9-615c-442e-ab9b-bd4fe7794024.css?ver=3.9.2' type='text/css' media='all' />
    <link href="/vendor/uncg/wrapper/rincuncg/css/common.min.css" rel="stylesheet" type="text/css" />
    <link href="/vendor/uncg/wrapper/css/wrapper-extras.min.css" rel="stylesheet" type="text/css" />

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    @if(config('uncgtheme.custom_css'))
        <link href="{{ mix('uncg/css/' . config('uncgtheme.custom_css')) }}" rel="stylesheet" type="text/css" />
    @endif

    <link href="/vendor/uncg/wrapper/rincuncg/css/horizontal-nav.min.css" rel="stylesheet" type="text/css" />
    <link href="/vendor/uncg/wrapper/rincuncg/css/print.min.css" rel="stylesheet" type="text/css" media="print" />
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script type='text/javascript' src='/vendor/uncg/wrapper/rincuncg/js/bootstrap.min.js?ver=3.9.2'></script>
    <script type="text/javascript" src="/vendor/uncg/wrapper/rincuncg/js/common.min.js"></script>

    @if(config('uncgtheme.page_header'))
        @include(config('uncgtheme.page_header'))
    @endif

    <script type="text/javascript">/* Google Analytics */
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-34361385-1']);
        _gaq.push(['_setDomainName', '.uncg.edu']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>

    <!--[if lt IE 9]>
    <script type="text/javascript" src="/vendor/uncg/wrapper/rincuncg/js/lt-ie-9.min.js"></script>
    <link type="text/css" rel="stylesheet" charset="UTF-8" href="/vendor/uncg/wrapper/rincuncg/css/lt-ie-9.min.css"/>
    <![endif]-->
</head>
<body ontouchstart="">
<ul id="uncgAccessNav">
    <li><a href="#startcontent">Skip to Main Content</a></li>
</ul>

<?php require_once(public_path() . '/vendor/uncg/wrapper/rincuncg/heading.php'); ?>

<div class="shadow-box">
    <div class=""><a name="startcontent"></a>

        <div class="main-content ">

            @if(config('uncgtheme.unit_goldbar'))
                {{-- Manual menu --}}
                @include(config('uncgtheme.unit_goldbar'))
            @else
                {{-- Built in menu (using Lavary package) --}}
                @include('components.ccps_menu')
            @endif

            <div>
                <article>
                    <div class="container no-bottom-padding ">


                        {{-- display header block if defined --}}
                        @if(config('uncgtheme.unit_sub_title') || config('uncgtheme.dept_name'))
                            <div class="row">
                                <div id="unit-head" class="row">
                                {!! config('uncgtheme.dept_name') ? '<h1 class="site-title">'. config('uncgtheme.dept_name') .'</h1>' : "" !!}
                                {!! config('uncgtheme.unit_sub_title') ? '<h2 class="site-title">'. config('uncgtheme.unit_sub_title') .'</h2>' : "" !!}
                                </div>
                            </div>
                        @endif


                        <div id="content-wrapper">
                        {{--determine which of the 4 layouts is being implemented--}}
                        @if(config('uncgtheme.unit_menu') && config('uncgtheme.page_sidebar'))
                            <div class="row for-sidebar">
                                <div id="content" class="main-content-inner col-xs-6 col-xs-push-3">
                        @elseif(config('uncgtheme.unit_menu'))
                            <div class="row">
                                <div id="content" class="main-content-inner  col-xs-9 col-xs-push-3">
                        @elseif(config('uncgtheme.page_sidebar'))
                            <div class="row for-sidebar">
                                <div id="content" class="main-content-inner col-xs-9">
                        @else
                            <div class="row">
                                <div id="content" class="main-content-inner col-xs-12">
                        @endif