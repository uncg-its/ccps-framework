</div>

    {{--add unit menu to page--}}
    @if(config('uncgtheme.unit_menu'))
        @if(config('uncgtheme.page_sidebar')))
            <div class="sidebar col-sm-3 col-sm-pull-6"><div class="sidebar-padder">
        @else
            <div class="sidebar col-sm-3 col-sm-pull-9"><div class="sidebar-padder">
        @endif

        @include(config('uncgtheme.unit_menu'))

        </div></div>
    @endif

    {{--add right sidebar to page--}}
    @if(config('uncgtheme.page_sidebar'))
        <div class="sidebar-right col-sm-3"><div class="sidebar-padder">
        @include(config('uncgtheme.page_sidebar'))
        </div></div>';
    @endif

            </div>
        </div>
        </div>
  </article>
</div>
    </div>


</div>
<div id="footer" class="site-footer" role="contentinfo">
   <div id="footer-content" class="row">
      <div id="footer-left" class="site-footer-inner-left col-xs-12 col-sm-4 col-md-6">
         @if(config('uncgtheme.unit_contact'))
             @include(config('uncgtheme.unit_contact'))
         @else
            <dl title="Contact Information">
                <dt><strong>The University of North Carolina at Greensboro</strong></dt>
                <dd>Greensboro, NC 27402-6170</dd>
            </dl>
         @endif


         @if(config('uncgtheme.admin_info'))
             @include(config('uncgtheme.admin_info'))
         @endif

      </div>
      <div id="footer-right" class="site-footer-inner-right col-md-6 hidden-xs">
        <?php require_once(public_path() . '/vendor/uncg/wrapper/rincuncg/footer.htm');?>
      </div>
   </div>
</div>
<div style="clear:both;display:block;height:0">&nbsp;</div>
</div>
<div id="shadow-bottom">&nbsp;</div>

@if(config('uncgtheme.custom_js'))
    <script src="{{ mix('uncg/js/' . config('uncgtheme.custom_js')) }}"></script>
@endif

@yield('scripts')

</body>
</html>
