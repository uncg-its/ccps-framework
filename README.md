---
# ARCHIVED 11/21/2018

Repo is no longer in use, abandonded in favor of [CCPS Core](https://bitbucket.org/uncg-its/ccps-core/src/master/)

---

# CCPS Framework

Authors / Contributors:

- Matt Libera (matt.libera@uncg.edu)
- Nick Young (n_young@uncg.edu)
- Kevin McClain (kevin_mcclain@uncg.edu)

## Introduction

This is an application framework built on Laravel (currently v5.4) designed to run a variety of applications at The University of North Carolina at Greensboro. It was built for use by the Cloud Collaboration and Productivity Services group in the Information Technology Services department at UNCG.

## Installation
> **This is currently a beta version**. Many, many, many things do not work properly yet, and are still showing Laravel defaults.

Currently this framework is only available from this repository. In the future the goal is to have it listed on Packagist for easy installation via Composer.
 
For now, to install:

1. Clone the repository
2. Remove this repo as a remote, and add your own
3. Create a database to be used for your app
4. `cp .env.example .env` to make your Laravel `.env` file, and edit that file to include your own app and database information
5. Run `composer install`
6. `php artisan key:generate` to get your application key
7. `php artisan migrate` to perform the database migrations
8. `php artisan db:seed --class=CcpsSeeder` to get the initial database information, including default user and default roles, etc.
9. `npm install` to get all required Javascript libraries for development (e.g. Laravel Mix)
10. (OPTIONAL) If using PhpStorm IDE, run `php artisan ide-helper:generate` and then `php artisan ide-helper:meta` to help PhpStorm better understand the components of this app.
11. (RECOMMENDED) to install the UNCG Wrapper theme, run `php artisan uncgtheme:install`. This copies assets, creates symlinks, and creates a file inside `config` for you to edit specifically for your app.
12. Log in with the default admin account (below), and go to **Admin > Config** to set up initial application variables.

## Accounts / Logging in

The database seeder automatically creates an admin user with the following credentials:
 
 - username: *admin@admin.com*
 - password: *admin*

Log in with these credentials to access the application from an administrator account.

### Local accounts

By default the app is set to allow anyone to register with any email address, local accounts only.

### Socialite for 3rd party logins

CCPS currently supports the following OAuth providers for login out of the box:

- Google
- Azure

To enable Socialite for one / both of these, you will need to register an application with the service to get your client ID and secret - which can then be inserted into the `.env` file.

#### Restricting Socialite logins to one domain

You may restrict access to account creation / login to one specific domain if you wish (e.g. only allow users from *uncg.edu*). This is not enabled out of the box, but you may enable it. To enable this feature, set the `SOCIALITE_ALLOW_ONLY_DOMAIN` variable in your `.env` file to the domain you wish to use.

At the very least, this enables some checks in the app to block users from other domains. This is also incorporated into the OAuth request where supported (example, Google accepts the `hd` parameter, and enforces it directly on their login page).

#### Adding other providers

You may, of course, add other providers. It is recommended to install the appropriate library in Composer, and then modify your views and the `AuthController` accordingly to be able to handle these other providers. You'll also need additional `.env` variables - refer to Socialite documentation.
 

## Other notes
A better README file will probably show up sometime.